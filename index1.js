var NodePDF = require('nodepdf');
var pdf = new NodePDF('http://yahoo.com', 'yahoo.pdf', {
    'viewportSize': {
        'width': 3000,
        'height': 9000
    },
    'paperSize': {
        'pageFormat': 'A4',
        'margin': {
            'top': '2cm'
        },
        'header': {
            'height': '1cm',
            'contents': 'HEADER {currentPage} / {pages}' // If you have 2 pages the result looks like this: HEADER 1 / 2
        },
        'footer': {
            'height': '1cm',
            'contents': 'FOOTER {currentPage} / {pages}'
        }
    },
    'outputQuality': '80',
    'zoomFactor': 1.1
});