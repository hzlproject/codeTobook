/**
 * [exports 所有自定义过滤器]
 * @type {Object}
 */

/**
 * [int 取整过滤器]
 * @param  {[type]} str [原始字符串]
 * @return {[type]}     [返回['rate50']的形式]
 */
avalon.filters.int = function(str) {
    return [str[0].split('.')[0]];
};

/**
 * [status 主导航过滤]
 * @param  {[type]} str    [占位符]
 * @param  {[type]} urlArr [当前数组]
 * @return {[type]}        [description]
 */
avalon.filters.status = function(str, urlArr, number, index) {
    var pathname = window.location.pathname,
        suffix = window.location.search,
        finalUrl = pathname + suffix,
        search = window.location.search;
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(elt /*, from*/ ) {
            var len = this.length >>> 0;
            var from = Number(arguments[1]) || 0;
            from = (from < 0) ? Math.ceil(from) : Math.floor(from);
            if (from < 0) {
                from += len;
                for (; from < len; from++) {
                    if (from in this &&
                        this[from] === elt)
                        return from;
                }
                return -1;
            }
        };
    }
    if (number == 1) { //主导航
        if (urlArr.indexOf(pathname) != -1) {
            return 'active';
        } else {
            return '';
        }
    } else if (number == 2) { //关于我们-子导航
        if (!suffix) {
            if (urlArr == pathname) {
                return 'current';
            } else {
                return '';
            }
        } else {
            if (urlArr == finalUrl) {
                return 'current';
            } else {
                return '';
            }
        }
    } else if (number == 3) { //会员中心-子导航
        if (urlArr == pathname || pathname == '/') {
            return 'active';
        } else {
            return '';
        }
    } else if (number == 4) { //会员中心-三级导航
        if (!suffix) {
            if (search == '' && index == 0) {
                return 'active';
            }
            if (urlArr == pathname) {
                return 'active';
            } else {
                return '';
            }
        } else {
            if (urlArr == finalUrl) {
                return 'active';
            } else {
                return '';
            }
        }
    }
};

/**
 * [num 首页理财列表期限过滤]
 * @param  {[type]} str [原始字符串]
 * @param  {[type]} type [类型]
 * @param  {[type]} n   [字符串截取位数]
 * @return {[type]}     [截取之后字符串]
 */

avalon.filters.num = function(str, type, n) {
    var newstr = '';
    if (type == 'time') {
        newstr = str.substring(n);
    } else {
        newstr = str.substring(0, str.length - n);
    }
    return newstr;
};



/**
 * [numberdate 列表日期转换]
 * @param  {[type]} str [原始字符串]
 * @return {[type]}     [返回13位日期形式]
 */
avalon.filters.numberdate = function(str) {
    if (str === '' || str == null) {
        return '';
    }
    return parseInt(str) * 1000;
};

/**
 * [formatMoney 格式化金额,可以控制小数位数，自动四舍五入]
 * @param  {[type]} money [金额]
 * @param  {[type]} num   [小数的位数]
 * @return {[type]}       [description]
 */
avalon.filters.formatMoney = function(money, num) {
    num = num > 0 && num <= 20 ? num : 2;
    money = parseFloat((money + '').replace(/[^\d\.-]/g, '')).toFixed(num) + '';

    if (isNaN(money) || ((money + '').replace(/\s/g, '')) === '') {
        return '';
    }

    var leftNum = money.split('.')[0].split('').reverse(),
        rightNum = money.split('.')[1];

    var temp = '';
    var i;
    for (i = 0; i < leftNum.length; i++) {
        temp += leftNum[i] + ((i + 1) % 3 === 0 && (i + 1) != leftNum.length ? ',' : '');
    }

    return temp.split('').reverse().join('') + '.' + rightNum;
};

/**
 * [addSpace 增加空格]
 * @param {[type]} number [description]
 */
avalon.filters.addSpace = function(number) {
    return number.replace(/\D/g, '').replace(/\s+/g, '').replace(/(\d{4})(?=\d)/g, '$1 ');
};
