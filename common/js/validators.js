var avalon = require('avalon2');

/**
 * [checkPassword 注册密码规则检测]
 * @type {Object}
 */
avalon.validators.checkPassword = {
    message: '密码必须是6-20位字母和数字组合',
    get: function (value, field, next) {
        if (/^\d+$/.test(value) || /^[a-zA-Z]+$/.test(value)) {
            next(false);
        }else if(!/^[A-Za-z0-9\~\!\@\#\$\%\^\*\(\)\-\_\=\+\[\]\/,;\.\']{6,20}$/.test(value)){
            next(false);
        }else{
            next(value);
            return value;
        }
    }
};

avalon.validators.checked = {
    message: '请阅读并同意注册协议',
    get: function (value, field, next) {
        next(value)
        return value
    }
};